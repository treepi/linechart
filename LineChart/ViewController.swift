//
//  ViewController.swift
//  LineChart
//
//  Created by k ely on 3/5/15.
//  Copyright (c) 2015 flyingfresh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  var lineChart: LineChart!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    lineChart = LineChart(frame: CGRectMake(0, 200, view.frame.size.width, 250))
    view.addSubview(lineChart)
    self.lineChart.backgroundColor = UIColor.lightGrayColor()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
}

