//
//  Utility.swift
//  Chartz
//
//  Created by k ely on 1/12/15.
//  Copyright (c) 2015 flyingfresh. All rights reserved.
//

import Foundation
import UIKit

struct Filter {
  var name: String
  var fillColor: UIColor
  var strokeColor: UIColor
  var opacity: Float
  var lineWidth: CGFloat
  var lineJoine: String
}

func center(view: UIView) -> CGPoint {
  return CGPoint(x: view.frame.size.width/2, y: view.frame.size.height/2)
}

// helper functions to animate for a keypath with generic value types, however using extention on CALayer limits your freedom to group multiple animations with more specific naming, in general it is recommended to use the functions only depending on your need.
extension CALayer {
  func addAnimationWithOptions<T: NSObject> (
    #keyPath: String,
    duration: Double? = nil,
    fromValue: T? = nil,
    toValue: T? = nil,
    timingFunction: CAMediaTimingFunction? = nil,
    fillMode: String? = nil,
    removedOnCompletion: Bool? = nil,
    beginTime: NSTimeInterval? = nil,
    delay: NSTimeInterval? = nil,
    speed: Float? = nil) {
      
      let animation = CABasicAnimation(keyPath: keyPath)
      
      // unwrapping optionals
      if let duration = duration {
        animation.duration = duration }
      if let fromValue = fromValue {
        animation.fromValue = fromValue }
      if let toValue = toValue {
        animation.toValue = toValue }
      if let timingFunction = timingFunction {
        animation.timingFunction = timingFunction }
      if let fillMode = fillMode {
        animation.fillMode = fillMode }
      if let removedOnCompletion = removedOnCompletion {
        animation.removedOnCompletion = removedOnCompletion }
      if let beginTime = beginTime {
        animation.beginTime = beginTime }
      if let delay = delay {
        animation.beginTime = CACurrentMediaTime() + delay }
      if let speed = speed {
        animation.speed = speed }
      
      self.addAnimation(animation, forKey: keyPath)
  }
  
  func addKeyFrameAnimationWithOptions<T: NSObject>(
    #keyPath: String,
    duration: Double? = nil,
    values: [T]? = nil,
    calculationMode: String? = nil,
    timingFunction: CAMediaTimingFunction? = nil,
    timingFunctions: [CAMediaTimingFunction]? = nil,
    path: CGPath? = nil,
    fillMode: String? = nil,
    removedOnCompletion: Bool? = nil,
    delay: NSTimeInterval? = nil,
    additive: Bool? = nil,
    rotationMode: String? = nil,
    speed: Float? = nil) {
      
      let animation = CAKeyframeAnimation(keyPath: keyPath)
      
      // unwrapping optionals
      if let duration = duration {
        animation.duration = duration
      }
      if let values = values {
        animation.values = values
      }
      if let calculationMode = calculationMode {
        animation.calculationMode = calculationMode
      }
      if let timingFunction = timingFunction {
        animation.timingFunction = timingFunction
      }
      if let timingFunctions = timingFunctions {
        animation.timingFunctions = timingFunctions
      }
      if let path = path {
        animation.path = path
      }
      if let fillMode = fillMode {
        animation.fillMode = fillMode
      }
      if let removedOnCompletion = removedOnCompletion {
        animation.removedOnCompletion = removedOnCompletion
      }
      if let delay = delay {
        animation.beginTime = CACurrentMediaTime() + delay
      }
      if let additive = additive {
        animation.additive = additive
      }
      if let rotationMode = rotationMode {
        animation.rotationMode = rotationMode
      }
      if let speed = speed {
        animation.speed = speed
      }
      
      self.addAnimation(animation, forKey: keyPath)
  }
}

extension Double {
  var radians: CGFloat {
    let degreeToRadiansFloatValue = self * M_PI / 180
    return CGFloat(degreeToRadiansFloatValue)
  }
  func toString() -> String {
    return String(format: "%.2f",self)
  }
}
