//
//  LineChart.swift
//  LineChart
//
//  Created by k ely on 3/5/15.
//  Copyright (c) 2015 flyingfresh. All rights reserved.
//

import UIKit

class LineChart: UIView {
  
  var scores = [Double]()
  var unitPoint: CGPoint?
  var currentDelay: NSTimeInterval = 0.0
  var duration: NSTimeInterval = 0.0
  var zeroPoint: CGPoint!
  var maxXPoint: CGPoint!
  var maxYPoint: CGPoint!
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    zeroPoint = CGPoint(x: frame.size.width/8, y: frame.size.height-(frame.size.height/8))
    maxXPoint = CGPoint(x: frame.size.width-(frame.size.width/8), y: frame.size.height-(frame.size.height/8))
    maxYPoint =  CGPoint(x: frame.size.width/8, y: frame.size.height/8)
    addAxesLayer()
    //    addDataLayers()
  }
  
  required init(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func applyFilter(filter: Filter, to layer: CAShapeLayer) {
    layer.name = filter.name
    layer.fillColor = filter.fillColor.CGColor
    layer.opacity = filter.opacity
    layer.strokeColor = filter.strokeColor.CGColor
    layer.lineWidth = filter.lineWidth
    layer.lineJoin = filter.lineJoine
  }
  
  func addAxesLayer() {
    
    let xAxesLayer = CAShapeLayer()
    let yAxesLayer = CAShapeLayer()
    
    let xAxesLayerFilter = Filter(name: "AxesLayer", fillColor: UIColor.clearColor(), strokeColor: UIColor.grayColor(), opacity: 1.0, lineWidth: 2, lineJoine: kCALineJoinMiter)
    applyFilter(xAxesLayerFilter, to: xAxesLayer)
    let yAxesLayerFilter = Filter(name: "AxesLayer", fillColor: UIColor.clearColor(), strokeColor: UIColor.grayColor(), opacity: 1.0, lineWidth: 2, lineJoine: kCALineJoinMiter)
    applyFilter(yAxesLayerFilter, to: yAxesLayer)
    
    let xAxesPath = UIBezierPath()
    xAxesPath.moveToPoint(zeroPoint)
    xAxesPath.addLineToPoint(maxXPoint)
    xAxesPath.closePath()
    let yAxesPath = UIBezierPath()
    yAxesPath.moveToPoint(zeroPoint)
    yAxesPath.addLineToPoint(maxYPoint)
    yAxesPath.closePath()
    
    xAxesLayer.path = xAxesPath.CGPath
    yAxesLayer.path = yAxesPath.CGPath
    
    layer.addSublayer(xAxesLayer)
    layer.addSublayer(yAxesLayer)
    
    xAxesLayer.addAnimationWithOptions(keyPath: "strokeEnd", duration: 4, fromValue: 0.0, toValue: 1.0, timingFunction: CAMediaTimingFunction(name: kCAMediaTimingFunctionDefault))
    yAxesLayer.addAnimationWithOptions(keyPath: "strokeEnd", duration: 4, fromValue: 0.0, toValue: 1.0, timingFunction: CAMediaTimingFunction(name: kCAMediaTimingFunctionDefault))
  }
  
  // CONTINUE...
  //  func addDataLayers(){
  //    for score in scores {
  //      let unit = (maxXPoint.x - zeroPoint.x) / CGFloat(scores.count)
  //    }
  //  }
}
